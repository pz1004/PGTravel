## Deterministic Policy Gradient Algorithms

### 3. Gradient of Deterministic Policies

- regularity conditions A.1
    - \( p(s'|s,a), \bigtriangledown_{a}p(s'|s,a), \mu_{\theta}(s), \bigtriangledown_{\theta}\mu_{\theta}(s), r(s,a), \bigtriangledown_{a}r(s,a), p_{1}(s) \) are continuous in all parameters and variables \( s, a, s' \) and \( x \).
    - 의미:
        - \( \bigtriangledown_{a}Q^{\mu}(s,a) \) 가 존재함.
- regularity conditions A.2
    - there exists a \( b \) and \( L \) such that \( \sup_{s}p_{1}(s) < b \), \( \sup_{a,s,s'}p(s'|s,a) < b \), \( \sup_{a,s}r(s,a) < b \), \( \sup_{a,s,s'}\|\bigtriangledown_{a}p(s'|s,a)\| < L \), and \( \sup_{a,s}\|\bigtriangledown_{a}r(s,a)\| < L \).
    - 의미:
        - 값들이 bounded 되어 있음
1) 주 contribution
    - deterministic policy gradient 형태에 대한 informal intuition 제공
        - generalized policy iteration
        - 정책 평가와 정책 발전이 번갈아가면서 수행되는 정책 iteration
    - 정책 평가
        - action-value function \( Q^{\pi}(s,a) \) or \( Q^{\mu}(s,a) \) 을 estimate 하는 것
        - 주로 Monte-Carlo 평가나 temporal-difference learning 사용함
    - 정책 발전
        - 위 estimated action-value function 에 따라 정책을 update 하는 것
        - 주로 action-value function 에 대한 greedy maximisation 사용함
            - \( \mu^{k+1}(s) = \arg\max\limits_{a}Q^{\mu^{k}}(s,a) \)
            - greedy 정책 발전은 매 단계마다 global maximization을 해야하는데, 이는 continuous action spaces 에서 큰 문제를 야기함.
        - 그렇기에 policy gradient 방법이 나옴
            - policy 를 \( \theta \) 에 대해서 parameterize 함
            - 매 단계마다 global maximisation 수행하는 대신, 방문하는 state \( s \) 마다 policy parameter 를 action-value function \( Q \) 의 \( \theta \) 에 대한 gradient \( \bigtriangledown_{\theta}Q^{\mu^{k}}(s,\mu_{\theta}(s)) \) 방향으로 proportional 하게 update 함
            - 하지만 각 state 는 다른 방향을 제시할 수 있기에, state distribution \( \rho^{\mu}(s) \) 에 대한 기대값을 취해 policy parameter 를 update 할 수도 있음
                - \( \theta^{k+1} = \theta^{k} + \alpha \rm I\!E_{s \sim \rho^{\mu^{k}}} [\bigtriangledown_{\theta}Q^{\mu^{k}}(s,\mu_{\theta}(s))] \)
            - 이는 chain-rule 에 따라 아래와 같이 분리될 수 있음
                - \( \theta^{k+1} = \theta^{k} + \alpha \rm I\!E_{s \sim \rho^{\mu^{k}}} [\bigtriangledown_{\theta}\mu_{\theta}(s)\bigtriangledown_{a}Q^{\mu^{k}}(s,a)|_{a=\mu_{\theta}(s)}] \) (7)
                - chain rule: \( \frac{\partial Q}{\partial \theta} = \frac{\partial a}{\partial \theta} \frac{\partial Q}{\partial a} \)
            - 하지만 state distribution \( \rho^{\mu} \) 은 정책에 dependent 함
                - 정책이 바꾸게 되면, 바뀐 정책에 따라 방문하게 되는 state 가 변하게 되기 때문에 state distribution이 변하게 됨
            - 그렇기에 정책 update 시 state distribution에 대한 gradient 를 고려하지 않는데 정책 발전이 이뤄진다는 것은 직관적으로 와닿지 않을 수 있음
            - deterministic policy gradient theorem 은 state distribution에 대한 gradient 계산없이 위 식(7) 대로만 update 해도 performance objective 의 gradient 를 정확하게 따름을 의미한다.
    - deterministic policy gradient theorem 에 대한 증명
        - deterministic Policy
            - \( \mu_{\theta} : S \to A \) with parameter vector \( \theta \in \rm I \! R^n \)
        - probability distribution
            - \( p(s \to s', t, \mu) \)
        - discounted state distribution
            - \( \rho^{\mu}(s) \)
        - performance objective
            - \( J(\mu_{\theta}) = \rm I \! E[r^{\gamma}_{1}|\mu] \)
            - \( \begin{eqnarray}
                    J(\mu_{\theta}) &=& \int_{S}\rho^{\mu}(s)r(s,\mu_{\theta}(s))ds \nonumber \\
                                &=& \rm I \! E_{s \sim \rho^{\mu}}[r(s,\mu_{\theta}(s))]
                \end{eqnarray} \)
        - Deterministic Policy Gradient Theorem
            - MDP 가 A.1 만족한다면, 아래 식(9)이 성립함
            - \( \begin{eqnarray}
                \bigtriangledown_{\theta}J(\mu_{\theta}) &=& \int_{S}\rho^{\mu}(s)\bigtriangledown_{\theta}\mu_{\theta}(s)\bigtriangledown_{a}Q^{\mu}(s,a)|_{a=\mu_{\theta}(s)}ds \nonumber \\
                        &=& \rm I \! E_{s \sim \rho^{\mu}}[\bigtriangledown_{\theta}\mu_{\theta}(s)\bigtriangledown_{a}Q^{\mu}(s,a)|_{a=\mu_{\theta}(s)}]   \nonumber \\
            \end{eqnarray} \) (9)
    - deterministic policy gradient theorem 이 stochastic policy gradient theorem 의 limiting case 임을 보임
        - deterministic policy gradient 가 stochastic policy gradient 의 특수 case 임
        - stochastic policy parameterization
            - \( \pi_{\mu_{\theta},\sigma} \) by a deterministic policy \( \mu_{\theta} : S \to A \) and a variance parameter \( \sigma \)
            - \( \sigma = 0 \) 이면, \( \pi_{\mu_{\theta},\sigma} \equiv \mu_{\theta} \)
        - Theorem 2. As \( \sigma \to 0 \), the stochastic policy gradient converges to the deterministic policy gradient.
            - 내용
        - 의미 :
            - 기존 유명한 policy gradients 기법들에 deterministic policy gradients 를 적용할 수 있음
                - 기존 기법들 예: compatible function approximation (Sutton, 1999), natural gradients (Kakade, 2001), actor-critic (Bhatnagar, 2007) or episodic/batch methods (Peters, 2005)

### 4. Deterministic Actor-Critic Algorithms
1. 살사 critic 을 이용한 on-policy actor-critic
    - 단점
        - deterministic policy 에 의해 행동하면 exploration 이 잘 되지 않기에, sub-optimal 에 빠지기 쉬움
    - 목적
        - 교훈/정보제공
        - 환경에서 충분한 noise 를 제공하여 exploration을 시킬 수 있다면, deterministic policy 를 사용한다고 하여도 좋은 학습 결과를 얻을 수도 있음
            - 예. 바람이 agent의 행동에 영향(noise)을 줌
    - Remind: 살사(SARSA) update rule
        - \( Q(s_{t},a_{t}) \leftarrow Q(s_{t},a_{t}) + \alpha(r_{t} + \gamma Q(s_{t+1},a_{t+1}) - Q(s_{t},a_{t})) \)
    - Algorithm
        - critic 은 MSE 를 \( \bf minimize \) 하는 방향으로 action-value function 을 stochastic gradient \( \bf descent \) 방법으로 update 함
            - \( MSE = [Q^{\mu}(s,a) - Q^{w}(s,a)]^2 \)
                - critic 은 실제 \( Q^{\mu}(s,a) \) 대신 미분 가능한 \( Q^{w}(s,a) \) 로 대체하여 action-value function 을 estimate 하며, 이 둘 간 Mean Square Error 를 최소화하는 것이 목표
            - \( \bigtriangledown_{w}MSE \approx -2 * [r + \gamma Q^{w}(s',a') - Q^{w}(s,a)]\bigtriangledown_{w}Q^{w}(s,a)  \)
                - \( \bigtriangledown_{w}MSE = -2 * [Q^{\mu}(s,a) - Q^{w}(s,a)]\bigtriangledown_{w}Q^{w}(s,a)  \)
                - \( Q^{\mu}(s,a) \) 를 \( r + \gamma Q^{w}(s',a') \) 로 대체
                    - \( Q^{\mu}(s,a) = r + \gamma Q^{\mu}(s',a') \)
            - \( w_{t+1} = w_{t} + \alpha_{w}\delta_{t}\bigtriangledown_{w}Q^{w}(s_{t},a_{t}) \)
                - \( \delta_{t} = r_{t} + \gamma Q^{w}(s_{t+1},a_{t+1}) - Q^{w}(s_{t},a_{t}) \)
        - actor 는 식(9)에 따라 보상이 \( \bf maximize \) 되는 방향으로 deterministic policy 를 stochastic gradient \( \bf ascent \) 방법으로 update함
            - \( \theta_{t+1} = \theta_{t} + \alpha_{\theta} \bigtriangledown_{\theta}\mu_{\theta}(s_{t})\bigtriangledown_{a}Q^{w}(s_{t},a_{t})|_{a=\mu_{\theta}(s)} \)
2. Q-learning 을 이용한 off-policy actor-critic
    - stochastic behavior policy \( \beta(a|s) \) 에 의해 생성된 trajectories 로부터 deterministic target policy \( \mu_{\theta}(s) \) 를 학습하는 off-policy actor-critic
    - performance objective
        - \( \begin{eqnarray}
            J_{\beta}(\mu_{\theta}) &=& \int_{S}\rho^{\beta}(s)V^{\mu}(s)ds \nonumber \\
                    &=& \int_{S}\rho^{\beta}(s)Q^{\mu}(s,\mu_{\theta}(s))ds \nonumber \\
                    &=& \rm I \! E_{s \sim \rho^{\beta}}[Q^{\mu}(s,\mu_{\theta}(s))]
            \end{eqnarray} \)
    - off-policy deterministic policy gradient
        - \( \begin{eqnarray}
            \bigtriangledown_{\theta}J_{\beta}(\mu_{\theta}) &\approx& \int_{S}\rho^{\beta}(s)\bigtriangledown_{\theta}\mu_{\theta}(a|s)Q^{\mu}(s,a)ds \nonumber \\
                &=& \rm I \! E_{s \sim \rho^{\beta}}[\bigtriangledown_{\theta}\mu_{\theta}(s)\bigtriangledown_{a}Q^{\mu}(s,a)|_{a=\mu_{\theta}(s)}]
        \end{eqnarray} \)
        - 위 식 중 approximation 부분은 오류로 판단됨. action이 deterministic 이기에 action 에 대한 평균을 구할 필요가 없기에, 곱의 미분이 있을 필요가 없고, approximation을 할 필요가 사라짐. 즉, 그냥 아래와 같이 써도 될 듯 함.
        - \( \begin{eqnarray}
            \bigtriangledown_{\theta}J_{\beta}(\mu_{\theta})
                &=& \rm I \! E_{s \sim \rho^{\beta}}[\bigtriangledown_{\theta}\mu_{\theta}(s)\bigtriangledown_{a}Q^{\mu}(s,a)|_{a=\mu_{\theta}(s)}]
        \end{eqnarray} \)
    - Remind: 큐러닝(Q-learning) update rule
        - \( Q(s_{t},a_{t}) \leftarrow Q(s_{t},a_{t}) + \alpha(r_{t} + \gamma \max\limits_{a}Q(s_{t+1},a) - Q(s_{t},a_{t})) \)
    - algorithm: OPDAC (Off-Policy Deterministic Actor-Critic)
        - 살사를 이용한 on-policy deterministic actor-critic 과 아래 부분을 제외하고는 같음
            - target policy 는 \( \beta(a|s) \) 에 의해 생성된 trajectories 를 통해 학습함
            - 업데이트 목표 부분에 실제 행동 값 \( a_{t+1} \) 이 아니라 정책으로부터 나온 행동 값 \( \mu_{\theta}(s_{t+1}) \) 사용함
        - \( \delta_{t} = r_{t} + \gamma Q^{w}(s_{t+1},\mu_{\theta}(s_{t+1})) - Q^{w}(s_{t},a_{t}) \)
        - \( w_{t+1} = w_{t} + \alpha_{w}\delta_{t}\bigtriangledown_{w}Q^{w}(s_{t},a_{t}) \)
        - \( \theta_{t+1} = \theta_{t} + \alpha_{\theta} \bigtriangledown_{\theta}\mu_{\theta}(s_{t})\bigtriangledown_{a}Q^{w}(s_{t},a_{t})|_{a=\mu_{\theta}(s)} \)
    - stochastic off-policy actor-critic 은 대개 actor 와 critic 모두 importance sampling을 필요로 하지만, deterministic policy gradient 에선 importance sampling이 필요없음
        - actor 는 deterministic 이기에 action 에 대한 평균을 취할 필요가 없어져서 importance sampling 이 필요없어짐
            - stochastic vs. deterministic performance objective
                - stochastic : \( J_{\beta}(\mu_{\theta}) = \int_{S}\int_{A}\rho^{\beta}(s)\pi_{\theta}(a|s)Q^{\pi}(s,a)dads \)
                - deterministic : \( J_{\beta}(\mu_{\theta}) = \int_{S}\rho^{\beta}(s)Q^{\mu}(s,\mu_{\theta}(s))ds \)
            - performance objective 를 value function of the target policy 로 사용하기 때문에, 필요가 없어지는 것인데, 왜 그런지는 좀 더 논문을 봐야함
        - critic 은 importance sampling이 필요없는 off policy 알고리즘인 q learning 을 사용함
3. compatibl function approximation 및 gradient temporal-difference learning 을 이용한 actor-critic
    - 위 1, 2 는 아래와 같은 문제가 존재
        - function approximator 에 의한 bias
            - 일반적으로 \( Q^{\mu}(s,a) \) 를 \( Q^{w}(s,a) \) 로 대체하여 deterministic policy gradient 를 구하면, 그 gradient 는 실제로 아예 ascent 하는 방향이 아닐 수도 있음
        - off-policy learning 에 의한 instabilities
    - 그래서 stochastic 처럼 \( \bigtriangledown_{a}Q^{\mu}(s,a) \) 를 \( \bigtriangledown_{a}Q^{w}(s,a) \) 로 대체해도 deterministic policy gradient 에 영향을 미치지 않을 compatible function approximator \( Q^{w}(s,a) \) 를 찾아야 함
    - Theorem 3.
        - 아래 두 조건을 만족하면, \( Q^(w)(s,a) \) 는 deterministic policy \( \mu_{\theta}(s) \) 와 compatible 함. 즉, \( \bigtriangledown_{\theta}J_{\beta}(\mu_{\theta})
            = \rm I \! E_{s \sim \rho^{\beta}}[\bigtriangledown_{\theta}\mu_{\theta}(s)\bigtriangledown_{a}Q^{w}(s,a)|_{a=\mu_{\theta}(s)}] \)
            - 1. \( \bigtriangledown_{a}Q^{w}(s,a)|_{a=\mu_{\theta}(s)} = \bigtriangledown_{\theta}\mu_{\theta}(s)^{\top}w \)
            - 2. w 는 \( MSE(\theta, w) = \rm I \! E[\epsilon(s;\theta,w)^{\top}\epsilon(s;\theta,w)] \)
                - \( \epsilon(s;\theta,w) = \bigtriangledown_{a}Q^{w}(s,a)|_{a=\mu_{\theta}(s)} - \bigtriangledown_{a}Q^{\mu}(s,a)|_{a=\mu_{\theta}(s)}  \)
    - on-policy 뿐만 아니라 off-policy 에도 적용 가능하다.
    - 어떠한 deterministic policy 에 대해서도 \( Q^{w}(s,a) = (a-\mu_{\theta}(s))\top\bigtriangledown_{\theta}\mu_{\theta}(s)\top w + V^{v}(s) \) 형태의 compatible function approximator 가 존재함.
        - 앞의 term 은 advantage 를, 뒤의 term 은 value 를 의미
    - COPDAC-Q algorithm (Compatible Off-Policy Deterministic Actor-Critic Q-learning critic)
        - critic: \( \phi(s,a) = a\top\bigtriangledown_{\theta}\mu_{\theta} \) features 로부터 action-value function 을 estimate 하는 linear function approximator
            - behavior policy \( \beta(a|s) \) 로부터 얻은 samples를 이용하여 Q-learning 이나 gradient Q-learning 과 같은 off-policy algorithm 으로 학습 가능함
        - actor: 위 critic의 action-value gradient 방향으로 parameter update
        - \( \delta_{t} = r_{t} + \gamma Q^{w}(s_{t+1},\mu_{\theta}(s_{t+1})) - Q^{w}(s_{t},a_{t}) \)
        - \( \theta_{t+1} = \theta_{t} + \alpha_{\theta} \bigtriangledown_{\theta}\mu_{\theta}(s_{t})(\bigtriangledown_{\theta}\mu_{\theta}(s_{t})\top w_{t}) \)
        - \( w_{t+1} = w_{t} + \alpha_{w}\delta_{t}\phi(s_{t},a_{t}) \)
        - \( v_{t+1} = v_{t} + \alpha_{v}\delta_{t}\phi(s_{t}) \)
    - off-policy Q-learning은 linear function approximation을 이용하면 diverge 할 수도 있음
        - 그렇기에 simple Q-learning 대신 다른 기법이 필요함.
    - 그렇기에 critic 에 gradient Q-learning 사용한 COPDAC-GQ (Gradient Q-learning critic) algorithm 제안
        - gradient temporal-difference learning 에 기반한 기법들은 true gradient descent algorithm 이며, converge가 보장됨. (Sutton, 2009)
            - 기본 아이디어는 stochastic gradient descent 로 Mean-squared projected Bellman error (MSPBE) 를 최소화하는 것
            - critic 이 actor 보다 빠른 time-scale 로 update 되도록 step size 들을 잘 조절하면, critic 은 MSPBE 를 최소화하는 parameters 로 converge 하게 됨
        - critic 에 gradient temporal-difference learning 을 사용한 논문 (Degris 2012b)
        - gradient temporal-difference learning 의 일종인 gradient Q-learning 제안한 논문 (Maei, 2010)
    - COPDAC-GQ algorithm
        - \( \delta_{t} = r_{t} + \gamma Q^{w}(s_{t+1},\mu_{\theta}(s_{t+1})) - Q^{w}(s_{t},a_{t}) \)
        - \( \theta_{t+1} = \theta_{t} + \alpha_{\theta} \bigtriangledown_{\theta}\mu_{\theta}(s_{t})(\bigtriangledown_{\theta}\mu_{\theta}(s_{t})\top w_{t}) \)
        - \( w_{t+1} = w_{t} + \alpha_{w}\delta_{t}\phi(s_{t},a_{t}) - \alpha_{w}\gamma\phi(s_{t+1}, \mu_{\theta}(s_{t+1}))(\phi(s_{t},a_{t})\top u_{t}) \)
        - \( v_{t+1} = v_{t} + \alpha_{v}\delta_{t}\phi(s_{t}) - \alpha_{v}\gamma\phi(s_{t+1})(\phi(s_{t},a_{t})\top u_{t}) \)
        - \( u_{t+1} = u_{t} + \alpha_{u}(\delta_{t}-\phi(s_{t}, a_{t})\top u_{t})\phi(s_{t}, a_{t}) \)
    - stochastic actor-critic 과 같이 매 time-step 마다 update 시 필요한 계산의 복잡도는 \( O(mn) \)
        - m 은 action dimensions, n 은 number of policy parameters
    - natural policy gradient 도 deterministic policy gradient 로 확장될 수 있음을 보임
        - \( M(\theta)^{-1}\bigtriangledown_{\theta}J(\mu_{\theta}) \) 는 어떠한 metric \( M(\theta) \) 에 대한 our performance objective (식(14)) 의 steepest ascent direction 임 (Toussaint, 2012)
        - natural gradient 는 Fisher information metric \( M_{\pi}(\theta) \) 에 대한 steepest ascent direction 임
            - Fisher information metric 은 policy reparameterization 에 대해 불변임 (Bagnell, 2003)
        - deterministic policies 에 대해 metric 으로 \( M_{\mu}(\theta) = \rm I \! E_{s \sim \rho^{\mu}}[\bigtriangledown_{\theta}\mu_{\theta}(s)\bigtriangledown_{\theta}\mu_{\theta}(s)^{\top}] \) 을 사용.
                - 이는 variance 가 0 인 policy 에 대한 Fisher information metric 으로 볼 수 있음
                - \( \frac{\bigtriangledown_{\theta}\pi_{\theta}(a|s)}{\pi_{\theta}(a|s)} \) 에서 policy variance 가 0 이면, 특정 s 에 대한 \( \pi_{\theta}(a|s) \) 만 1 이 되고, 나머지는 0 임
        - deterministic policy gradient theorem 과 compatible function approximation 을 결합하면 \( \bigtriangledown_{\theta}J(\mu_{\theta}) = \rm I \! E_{s \sim \rho^{\mu}}[\bigtriangledown_{\theta}\mu_{\theta}(s)\bigtriangledown_{\theta}\mu_{\theta}(s)^{\top}w] \) 이 됨
            - steepest ascent direction 은 \( M_{\mu}(\theta)^{-1}\bigtriangledown_{\theta}J_{\beta}(\mu_{\theta}) = w \) 이 됨
        - 이 알고리즘은 COPDAC-Q 혹은 COPDAC-GQ 에서 \( \theta_{t+1} = \theta_{t} + \alpha_{\theta} \bigtriangledown_{\theta}\mu_{\theta}(s_{t})(\bigtriangledown_{\theta}\mu_{\theta}(s_{t})\top w_{t}) \) 식을 \( \theta_{t+1} = \theta_{t} + \alpha_{\theta}w_{t} \) 로 바꿔주기만 하면 됨
